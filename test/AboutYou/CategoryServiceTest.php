<?php

namespace Tests\RPGBundle\Service;

use AboutYou\Service\CategoryService;
use PHPUnit\Framework\TestCase;


class CategoryServiceTest extends TestCase
{
    /**
     * @var CategoryService
     */
    private $service;

    const CATEGORY_ID = 17325;

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->service = new CategoryService();
    }

    /**
     * Test getProducts method
     */
    public function testGetProducts()
    {
        $data = $this->service->getProducts(self::CATEGORY_ID);
        self::assertArrayHasKey('message', $data);
    }
}
