<?php

namespace AboutYou\Service;


use AboutYou\Entity\Product;

class CategoryService implements CategoryServiceInterface
{
    protected $pathToData = __DIR__ . '/../../../data/';

    /**
     * This method should read from a data source (JSON in our case)
     * and return an unsorted list of products found in the data source.
     *
     * @param integer $categoryId
     *
     * @return \AboutYou\Entity\Product[]
     */
    public function getProducts($categoryId)
    {
        $jsonData = file_get_contents($this->pathToData . $categoryId . '.json');
        $data = @json_decode($jsonData, true);

        if (!is_array($data) && isset($data['products'])) {
            throw new \Exception('No valid data');
        }

        $products = [];
        foreach ($data['products'] as $id => $item) {
            $item['id'] = $id;
            $products[] = new Product($item);
        }
        return $products;
    }
}