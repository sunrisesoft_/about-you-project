<?php

namespace AboutYou\Entity;


class Product
{
    use EntityDataProcessor;

    /**
     * Id of the Product.
     *
     * @var int
     */
    public $id;

    /**
     * Name of the Product.
     *
     * @var string
     */
    public $name;

    /**
     * Description of the Product.
     *
     * @var string
     */
    public $description;

    /**
     * Unsorted list of Variants with their corresponding prices.
     *
     * @var \AboutYou\Entity\Variant[]
     */
    public $variants = [];


    public function __construct(array $input)
    {
        $this->validateInputArrayProperties($input);

        $this->validateArray($input, 'variants');

        $this->id = $input['id'];
        $this->name = $input['name'];
        $this->description = $input['description'];

        foreach ($input['variants'] as $id => $variantItem) {
            $variantItem['id'] = $id;
            $this->variants[] = new Variant($variantItem);
        }
    }
}
