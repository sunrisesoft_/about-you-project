<?php

namespace AboutYou\Entity;


class Variant
{
    /**
     * Id of the Variant.
     *
     * @var int
     */
    public $id;

    /**
     * Defines if the Variant is default for the product.
     *
     * @var bool
     */
    public $isDefault;

    /**
     * Defines if the Variant is Available or not.
     * 
     * @var bool
     */
    public $isAvailable;

    /**
     * Number of available items in stock.
     *
     * @var int
     */
    public $quantity;

    /**
     * Size of the Variant.
     *
     * @var mixed
     */
    public $size;

    /**
     * Variant price.
     * 
     * @var \AboutYou\Entity\Price
     */
    public $price;

    use EntityDataProcessor;

    public function __construct(array $input)
    {
        $this->validateInputArrayProperties($input);

        $this->validateArray($input, 'price');

        $this->id = $input['id'];
        $this->isDefault = $input['isDefault'];
        $this->isAvailable = $input['isAvailable'];
        $this->quantity = $input['quantity'];
        $this->size = $input['size'];
        $this->price = new Price($input['price']);
    }
}
