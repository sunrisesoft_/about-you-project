<?php

namespace AboutYou\Entity;


class Price
{
    use EntityDataProcessor;

    /**
     * Current price.
     *
     * @var int
     */
    public $current;

    /**
     * Old price.
     *
     * @var int|null
     */
    public $old;

    /**
     * Defines if the price is sale.
     *
     * @var bool
     */
    public $isSale;

    public function __construct(array $input)
    {
        $this->validateInputArrayProperties($input);

        $this->current = $input['current'];
        $this->old = $input['old'];
        $this->isSale = $input['isSale'];
    }

}
