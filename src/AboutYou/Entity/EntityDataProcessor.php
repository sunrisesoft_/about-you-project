<?php

namespace AboutYou\Entity;


trait EntityDataProcessor
{
    /**
     * @param array $input
     * @throws \Exception
     */
    protected function validateInputArrayProperties(array $input)
    {
        foreach (array_keys(get_object_vars($this)) as $item) {
            if (!array_key_exists($item, $input)) {
                throw new \Exception(get_class($this) . ': Input doesn\'t contain ' . $item);
            }
        }
        return true;
    }

    /**
     * @param array $input
     * @param string $key
     * @return bool
     * @throws \Exception
     */
    protected function validateArray(array $input, $key)
    {
        if (!is_array($input[$key])) {
            throw new \Exception('Property ' . $key . ' is not an array');
        }
        return true;
    }
}